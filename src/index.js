var mallhost = 'http://mall.newtouch.cn';
var listurl = `http://mall.newtouch.cn/index.php/gallery-15908--grid-0-3--grid-g.html`
var request = require('async-request')
var cheerio = require('cheerio')
var Db = require('./db')
var cate_ids = require('./cate_ids');
var isWin = process.platform === "win32";

var db=new Db({
    host: '127.0.0.1',
    user: isWin  ? 'root' : 'foo',
    password: isWin ? '1111' : 'password',
    port: '3306',
    database: 'mall'
});

var cluster = require('cluster');
const numCPUs = require('os').cpus().length*3;
var lens = cate_ids.length;
var avg = Math.floor(lens / numCPUs);


if (cluster.isMaster) {
    for (let i = 0; i < numCPUs; i++) {
         cluster.fork();
    }
} else {
    console.log('process ',cluster.worker.id)
    let start = (cluster.worker.id-1) * avg;
    let end = Math.min(start + avg, lens);
    console.log(start,end)
   // init(start, end)
}
// init();


async function init(start=0, end=1) {
    console.log('开始:' + new Date())
    try {
        console.log(await db.connect())
    } catch (e) {
        console.log(e);
        return;
    }
    for (var k = start; k < end; k++) {
        await getList(cate_ids[k], k)
    }

    console.log(await db.end())
    console.log('结束:' + new Date())
}


async function getList(cate_id, k) {
    let first_page_body = await getPageBody(cate_id, 1);
    let $ = cheerio.load(first_page_body);
    let max_page_number = parseInt($('[name=totalPage]').val());
    let index = 2;
    let first_page_products = getProductsFromHtml($);

    await saveData(first_page_products);
    console.log(`<${k}>,分类id-${cate_id},共${max_page_number}页,第1页,完成`)
    while (index <= max_page_number) {
        let _body = await getPageBody(cate_id, index);
        let _products = getProductsFromHtml(_body);
        await saveData(_products);
        console.log(`<${k}>,分类id-${cate_id},共${max_page_number}页,第${index}页,完成`)
        index++;
    }
}

async function getPageBody(cate_id, index) {
    try {
        let page_request = await request(`${mallhost}/index.php/gallery-${cate_id}--grid-0-${index}--grid-g.html`);
        return page_request.body
    } catch (e) {
        console.log(e)

        return ''
    }

}


function getProductsFromHtml(body) {
    let products = [];

    try {
        let $ = 'function' === typeof body ? body : cheerio.load(body);
        $('.items-gallery.itemsList').each(function () {
            if ($(this).find('del').length === 0) return;
            let data = {
                id: $(this).attr('product').trim() * 1,
                title: $(this).find('.entry-title').attr('title'),
                img: $(this).find('div.goodpic > a > img').attr('lazyload'),
                price: parseFloat($(this).find('.sell-price').text().trim().substr(1)),
                origin_price: parseFloat($(this).find('del').text().trim().substr(1)) || 0,
                first_cate: $('div.bread-crumbs > span:nth-child(3) > a').text().trim(),
                second_cate: $('div.bread-crumbs > span:nth-child(5) > a').text().trim(),
                third_cate: $('div.bread-crumbs > span.now').text().trim()
            }
            data.spread = (data.origin_price - data.price).toFixed(2) * 1;
            data.discount = ((data.spread / data.origin_price) * 100).toFixed(2) * 1;
            data.link = `http://mall.newtouch.cn/index.php/product-${data.id}.html`;
            console.log(data)

            products.push(data);
        });
    } catch (e) {
        console.log(e)
    }

    return products;
}


async function saveData(pros) {

    for (let data of pros) {
        let sql = `insert into product (
                            id,
                            title,
                            price,
                            origin_price,
                            spread,
                            discount,
                            link,
                            first_cate,
                            second_cate,
                            third_cate,
                            img
                        )
                        values ( 
                            ${data.id},
                            "${data.title.replace(/['"]/g,'')}",
                            ${data.price},
                            ${data.origin_price},
                            ${data.spread},
                            ${data.discount},
                            "${data.link.replace(/['"]/g,'')}",
                            "${data.first_cate.replace(/['"]/g,'')}",
                            "${data.second_cate.replace(/['"]/g,'')}",
                            "${data.third_cate.replace(/['"]/g,'')}",
                            "${data.img.replace(/['"]/g,'')}"
                            )`

        await db.query(sql)
    }

}

async function emptyProductTable() {
    try {
        console.log(await db.connect())
    } catch (e) {
        console.log(e);
        return;
    }
    await db.query('delete from product')
    console.log('清空产品表成功')
    console.log(await db.end())
}


async function exportProudctToCsv() {
    let alasql = require('alasql');
    try {
        console.log(await db.connect())
    } catch (e) {
        console.log(e);
        return;
    }
    await (async () => new Promise(async (resolve, reject) => {
        let data = await db.query('select * from product');
        alasql.promise('SELECT * INTO XLSX("restest280b.xlsx") FROM ?', data)
            .then(function (data) {
                console.log('数据保存成功');
                resolve(1)
            }).catch(function (err) {
                console.log('Error:', err);
            });
    }));

    console.log(await db.end())
}