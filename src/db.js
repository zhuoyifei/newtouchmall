var mysql = require('mysql');

class Db{
    constructor(config){
        this.config=config;
        this.connection= mysql.createConnection(this.config);
    }
    async connect() {
        return new Promise((resolve, reject) => {
            this.connection.connect(function (err) {
                if (err){
                    console.log(JSON.stringify(err))
                   throw err;
                } 
                resolve('数据库链接成功')
            });
        })
    }
    async  query(sql) {
        return new Promise((resolve, reject) => {
            this.connection.query(sql, function (err, rows, fields) {
                if (err){
                    console.log(JSON.stringify(err))
                    console.log('sql 语句--------')
                    console.log(sql)
                    console.log('sql 语句---------')
                   // throw err;
                } 
                resolve(rows)
            });
        })
    }
    async end() {
        return new Promise((resolve, reject) => {
            this.connection.end(function (err) {
                if (err){
                    console.log(JSON.stringify(err))
                    // throw err;
                } 
                resolve('数据库关闭')
            });
        })
    }
}

module.exports = Db;
